<?php
namespace Tki\Utility;


class HashUtility
{
    
    /**
     * Creates a 40 character long sha1 hash of given array values.
     * NB: Requires HASH_KEY (salt) to be defined.
     * @param type $arr
     * @return type
     * @throws \Exception
     */
    public static function array_hash($arr) {
        if(!defined('SITE_HASH_KEY')) {
            throw new \Exception('SITE_HASH_KEY not defined');
        }
		$out = '';
		$hash_str = '';
		reset($arr);
		while(list($k,$v) = each($arr)) {
            if(is_array($v)) {
                $hash_str .= serialize($v);
            } else {
                $hash_str .= (string) $v;
            }
		}
		$out = sha1(SITE_HASH_KEY.$hash_str);
		return $out;
	}
    
    /**
     * Generates a 96 character hash used for SITE_HASH_KEY in environment
     * @param string $text
     * @return string 96 chars 
     */
    public static function generate_hash_key($text) {
		return hash('sha384',$text);
	}
    

}

