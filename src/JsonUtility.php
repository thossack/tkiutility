<?php
namespace Tki\Utility;


class JsonUtility
{
    
    /**
     * Encodes array as JSON, with escaped quotes. Returns object by default, unless
     * $assoc is set to false.
     * @param array $arr
     * @param boolean $assoc - Return keyed object
     * @param boolean $escape - Allow control of escaping special characters.
     * @return string
     */
    public static function arrayToJson($arr,$assoc=true,$escape=true) {
        $string = ($assoc) ? json_encode($arr,JSON_HEX_APOS | JSON_HEX_QUOT  | JSON_FORCE_OBJECT)
            : json_encode($arr,JSON_HEX_APOS | JSON_HEX_QUOT);
        //debug($string);
        // Manually escape problematic items - on page use
        if($escape) {
            $string = static::escapeJsonString($string);
        }
        //debug($string);
        return $string;
    }
    
    /**
     * Escapes characters in JSON string.
     * @param string $string
     * @return string
     */
    public static function escapeJsonString($string) {
        //debug($string);
        // Manually escape problematic items - on page use
        $string = str_replace('\b', '\\\\b', $string);            // Backspace
        $string = str_replace('\t', '\\\\t', $string);            // Tab
        $string = str_replace('\n', '\\\\n', $string);            // New line
        $string = str_replace('\r', '\\\\r', $string);            // Carriage return
        $string = str_replace('\f', '\\\\f', $string);            // Form feed
        $string = str_replace('\u0022', '\\\\u0022', $string);    // Escaped quot
        return $string;
    }
    
    /**
     * Escapes characters in JSON string.
     * @param string $string
     * @return string
     */
    public static function unEscapeJsonString($string) {
        return str_replace('\\\\', '\\', $string);
    }

}

