<?php
namespace Tki\Utility;


class NumberUtility
{
    
   public static function format_currency($value,$allowNegative=false,$decimal='.',$separator=',')
    {
        $chars = '0-9.';
        if($allowNegative) {
            $chars .= '\-';
        }
        return number_format((double)preg_replace('/[^'. $chars .']/', '', $value), 2,$decimal,$separator);
    }

}

