<?php
namespace Tki\Utility;


class ArrayUtility
{
    
    
    /**
     * Get an item from an array or object using "dot" notation.
     *
     * @param  object|array $data
     * @param  string|int $key  (path with dot notation, or array path)
     * @param  mixed $default
     *
     * @return mixed
     */
    public static function data_get($data, $key, $default = null) {
        if (empty($data) || $key === '' || $key === null)
            return self::value($default);
        $object_get = function ($obj, $k) {
            return isset($obj->{$k}) ? $obj->{$k} : null;
        };
        $array_get = function ($arr, $k) {
            return (isset($arr[$k])) ? $arr[$k] : null;
        };

        $result = $data;
        $keys = is_array($key) ? $key : explode('.', $key);
        $numKeys = count($keys);
        $i = 1;
        foreach ($keys as $k) {
            if (is_object($result)) {
                $result = $object_get($result, $k);
            } elseif (is_array($result)) {
                $result = $array_get($result, $k);
            } else {
                $result = null;
                if ($i < $numKeys)
                    break;
            }
            ++$i;
        }
        return ($result !== null) ? $result : self::value($default);
    }
    
    public static function value($value) {
        return ($value instanceof Closure) ? $value() : $value;
    }
    
    
    /**
     * Explodes string and trims values
     * @param string $str
     * @param string $sep - Separator
     * @return array
     */
    public static function trim_explode($sep=',',$str) {
        return preg_split ("/[\s*$sep\s*]*$sep+[\s*$sep\s*]*/", $str);
    }

    /**
     * Explodes string and trims values
     * @param string $str
     * @param string $sep - Separator
     * @return array
     */
    public static function trim_implode($sep=',',$arr) {
        $arr = (array) $arr;
        foreach($arr as $k => &$v) {
            $v = trim($v);
            if(empty($v)) {
                unset($arr[$k]);
            }
        }

        return implode($sep,$arr);
    }
    

}

